const dataLink = 'https://sheets.googleapis.com/v4/spreadsheets/1BEEEFWR9oZ-FwWly90nU3ZH46hGpjH7udL5EFt05vV4/values/A2:LJ?key=AIzaSyCsMz7BLK4lgt0838FzUypEk-fJkP5wnPg'

export const state = () => ({
  alumniData: [],
  alumniDetail: []
})

export const mutations = {
  getAlumniData(state, data) {
    state.alumniData = data
  },
  getAlumniDetail(state, data) {
    state.alumniDetail = data
  }
}
export const actions = {
  async getAlumniData(context, data) {
    try {
      const response = await this.$axios.get(dataLink)
      context.commit('getAlumniData', response.data)
    } catch (error) {

    }
  },
  async getProfileByName(context, data) {
    try {
      const response = await this.$axios.get(dataLink)
      response.data.values.filter(item => {
        if(item[0].toLowerCase() === data) {
          context.commit('getAlumniDetail', item)
        }
      })
    } catch (error) {

    }
  }
}
